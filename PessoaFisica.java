
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

/**
 *
 * @author alunos
 */

     
public class PessoaFisica extends Fornecedor {
   
    private String cpf;
    
   
    
    public PessoaFisica(String nome, String telefones, String telefoneResidencial) {
        super(nome, telefones, telefoneResidencial);
        
    }
    
     public PessoaFisica(String nome, String telefones, String cpf, String telefoneResidencial) {
        super(nome, telefones, telefoneResidencial);
        this.cpf = cpf;
    }
     
     

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
}
