/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

/**
 *
 * @author alunos
 */
public class PessoaJuridica extends Fornecedor{
    private String cnpj;
    private String razaoSocial;

    
    
    public PessoaJuridica(String nome, String telefone, String cnpj, String telefoneResidencial){
        super(nome,telefone, telefoneResidencial);
        this.cnpj = cnpj;
        
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
}
