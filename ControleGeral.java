/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ControleGeral.java
 *
 * Created on 20/05/2014, 11:00:26
 */
package Controle;


import Controle.*;
import static java.awt.image.ImageObserver.WIDTH;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author alunos
 */
public class ControleGeral extends javax.swing.JFrame {
        
    ControleFornecedor controleFornecedor = new ControleFornecedor();
    Produto produto;
    ControleProduto controleProduto;
    PessoaFisica pessoaFisica;
    PessoaJuridica pessoaJuridica;
    Fornecedor umFornecedor;
    ArrayList<Produto> produtos = new ArrayList();
    Endereco endereco = new Endereco();
    ArrayList telefones = new ArrayList();
    String tipoFisico = "Pessoa Física";
    String tipoJuridico = "Pessoa Juridica";
    private boolean modoAlteracao;
    private boolean novoRegistro;
    
    /** Creates new form ControleGeral */
    public ControleGeral() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        Fornecedor = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldEndereco = new javax.swing.JTextField();
        jTextField9CEP = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jButtonSalvar = new javax.swing.JButton();
        jTextFieldBairro = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldCidade = new javax.swing.JTextField();
        jTextFieldTelefoneResidencial = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldComplemento = new javax.swing.JTextField();
        jTextFieldTelefoneCelular = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jCheckBoxCPF = new javax.swing.JCheckBox();
        jCheckBoxCNPJ = new javax.swing.JCheckBox();
        jTextFieldCPF = new javax.swing.JTextField();
        jLabelLogradouro = new javax.swing.JLabel();
        jTextFieldLogradouro = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextFieldNumero = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextFieldEstado = new javax.swing.JTextField();
        jTextFieldPais = new javax.swing.JTextField();
        jButtonPesquisar = new javax.swing.JButton();
        jButtonNovo = new javax.swing.JButton();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jLabel12 = new javax.swing.JLabel();
        jTextFieldNomeProduto = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldDescricaoProduto = new javax.swing.JTextField();
        ValorCompra = new javax.swing.JLabel();
        jTextFieldValorCompra = new javax.swing.JTextField();
        jLabelValorVenda = new javax.swing.JLabel();
        jTextFieldValorVenda = new javax.swing.JTextField();
        jTextFieldQuantidade = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableProduto = new javax.swing.JTable();

        jScrollPane1.setViewportView(jEditorPane1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel11.setText("jLabel11");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName("nome"); // NOI18N

        jLabel5.setText("Endereço");

        jTextFieldNome.setName("painelEntradaNome"); // NOI18N
        jTextFieldNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNomeActionPerformed(evt);
            }
        });

        jLabel10.setText("CEP");

        jLabel6.setText("Estado");

        jButtonSalvar.setText("SALVAR");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jTextFieldBairro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldBairroActionPerformed(evt);
            }
        });

        jLabel7.setText("Bairro");

        jLabel1.setText("Nome");
        jLabel1.setName("painelNome"); // NOI18N

        jLabel8.setText("Cidade");

        jLabel2.setText("Telefone Residencial");

        jLabel9.setText("Complemento");

        jLabel3.setText("Telefone Celular");

        jLabel4.setText("CPF/CNPJ");

        jCheckBoxCPF.setText("CPF");
        jCheckBoxCPF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxCPFActionPerformed(evt);
            }
        });

        jCheckBoxCNPJ.setText("CNPJ");
        jCheckBoxCNPJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxCNPJActionPerformed(evt);
            }
        });

        jLabelLogradouro.setText("Logradouro");

        jLabel14.setText("Numero");

        jLabel15.setText("Pais");

        jButtonPesquisar.setText("PESQUISAR");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });

        jButtonNovo.setText("NOVO");
        jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(118, 118, 118)
                        .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldEstado))
                            .addComponent(jLabel10)
                            .addComponent(jTextField9CEP, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabelLogradouro))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextFieldBairro)
                                    .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jTextFieldEndereco)
                                        .addComponent(jTextFieldCidade, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE))
                                    .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                                    .addComponent(jTextFieldPais, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)))
                            .addComponent(jTextFieldTelefoneCelular)
                            .addComponent(jTextFieldTelefoneResidencial)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jTextFieldCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxCPF)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jCheckBoxCNPJ)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(153, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonNovo)
                .addGap(18, 18, 18)
                .addComponent(jButtonPesquisar)
                .addGap(18, 18, 18)
                .addComponent(jButtonSalvar)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextFieldTelefoneResidencial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldTelefoneCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jCheckBoxCPF)
                            .addComponent(jCheckBoxCNPJ)))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)))
                .addGap(40, 40, 40)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabelLogradouro)
                    .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField9CEP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(177, 177, 177)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonSalvar)
                            .addComponent(jButtonPesquisar)
                            .addComponent(jButtonNovo)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jTextFieldEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7)
                            .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))))
                .addContainerGap())
        );

        Fornecedor.addTab("CADASTRO", jPanel2);

        jLabel12.setText("PRODUTO");
        jLayeredPane1.add(jLabel12);
        jLabel12.setBounds(20, 40, 90, 14);
        jLayeredPane1.add(jTextFieldNomeProduto);
        jTextFieldNomeProduto.setBounds(190, 30, 190, 20);

        jLabel13.setText("DESCRIÇÃO");
        jLayeredPane1.add(jLabel13);
        jLabel13.setBounds(20, 80, 90, 14);
        jLayeredPane1.add(jTextFieldDescricaoProduto);
        jTextFieldDescricaoProduto.setBounds(190, 80, 190, 20);

        ValorCompra.setText("VALOR DE COMPRA");
        jLayeredPane1.add(ValorCompra);
        ValorCompra.setBounds(20, 120, 150, 14);
        jLayeredPane1.add(jTextFieldValorCompra);
        jTextFieldValorCompra.setBounds(190, 120, 190, 20);

        jLabelValorVenda.setText("VALOR DE VENDA");
        jLayeredPane1.add(jLabelValorVenda);
        jLabelValorVenda.setBounds(20, 160, 140, 14);
        jLayeredPane1.add(jTextFieldValorVenda);
        jTextFieldValorVenda.setBounds(190, 160, 190, 20);
        jLayeredPane1.add(jTextFieldQuantidade);
        jTextFieldQuantidade.setBounds(190, 200, 190, 20);

        jLabel16.setText("QUANTIDADE");
        jLayeredPane1.add(jLabel16);
        jLabel16.setBounds(20, 200, 130, 14);

        jTableProduto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, "0", "0", "0"},
                {null, null, null, "0", "0", "0"},
                {null, null, null, "0", "0", "0"},
                {null, null, null, "0", "0", "0"}
            },
            new String [] {
                "NOME", "PRODUTO", "DESCRIÇÃO", "VALOR DE COMPRA", "VALOR DE VENDA", "QUANTIDADE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableProduto.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jTableProdutoAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jTableProduto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableProdutoMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTableProduto);

        jLayeredPane1.add(jScrollPane2);
        jScrollPane2.setBounds(0, 270, 760, 220);

        Fornecedor.addTab("FORNECEDOR", jLayeredPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Fornecedor)
                .addGap(79, 79, 79))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Fornecedor, javax.swing.GroupLayout.DEFAULT_SIZE, 530, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jTextFieldNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNomeActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldNomeActionPerformed

private void jCheckBoxCPFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxCPFActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jCheckBoxCPFActionPerformed

private void jCheckBoxCNPJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxCNPJActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jCheckBoxCNPJActionPerformed


private void salvarRegistro() {
    
        if(jCheckBoxCPF.isEnabled())
            
        {
            
            String nome = jTextFieldNome.getText();
            pessoaFisica.setNome(nome);

            String cpf = jTextFieldCPF.getText();
            pessoaFisica.setCpf(cpf);

            String telefone = jTextFieldTelefoneResidencial.getText();
            pessoaFisica.setTelefones(telefone);
            
            String telefoneCelular = jTextFieldTelefoneCelular.getText();
            pessoaFisica.setTelefones(telefoneCelular);
            
            
            endereco.setBairro(jTextFieldBairro.getText());
            endereco.setCidade(jTextFieldCidade.getText());
            endereco.setComplemento(jTextFieldComplemento.getText());
            endereco.setLogradouro(jTextFieldLogradouro.getText());
            endereco.setNumero(jTextFieldNumero.getText());
            endereco.setPais(jTextFieldPais.getText());
            endereco.setEstado(jTextFieldEstado.getText());

            pessoaFisica.setEndereco(endereco);
            
            produto.setNome(jTextFieldNomeProduto.getText());
            produto.setDescrição(jTextFieldDescricaoProduto.getText());
            
        
            pessoaFisica.setProdutos(produtos);
            

            
                double valorCompra = Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompra);
            
                double valorVenda = Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVenda);
            
                double quantidadeEstoque = Double.parseDouble(jTextFieldQuantidade.getText());
                produto.setQuantidadeEstoque(quantidadeEstoque);
            
                controleFornecedor.adiciona(pessoaFisica);
                
                
                controleProduto.adiciona(produto);
                
                controleProduto.carregarListaProdutos();
            
               
                JOptionPane.showMessageDialog(rootPane, jTextFieldNome.getText() + " adicionado(a) com sucesso", "Sucesso", WIDTH, null);
                
                produto = new Produto();
        
         
            
    
    
                    }
        
       else if(jCheckBoxCNPJ.isEnabled()){
            
            
            String nome = jTextFieldNome.getText();
            pessoaJuridica.setNome(nome);

            String cnpj = jTextFieldCPF.getText();
            pessoaJuridica.setCnpj(cnpj);

            String telefoneResidencial = jTextFieldTelefoneResidencial.getText();
            pessoaJuridica.setTelefones(telefoneResidencial);
            
            String telefoneCelular = jTextFieldTelefoneCelular.getText();
            pessoaJuridica.setTelefones(telefoneCelular);
            
            endereco.setBairro(jTextFieldBairro.getText());
            endereco.setCidade(jTextFieldCidade.getText());
            endereco.setComplemento(jTextFieldComplemento.getText());
            endereco.setLogradouro(jTextFieldLogradouro.getText());
            endereco.setNumero(jTextFieldNumero.getText());
            endereco.setPais(jTextFieldPais.getText());
            endereco.setEstado(jTextFieldEstado.getText());

            pessoaJuridica.setEndereco(endereco);
            
            produto.setNome(jTextFieldNomeProduto.getText());
            produto.setDescrição(jTextFieldDescricaoProduto.getText());
            
        
            pessoaJuridica.setProdutos(produtos);
            

            
                double valorCompra = Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompra);
            
                double valorVenda = Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVenda);
            
                double quantidadeEstoque = Double.parseDouble(jTextFieldQuantidade.getText());
                produto.setQuantidadeEstoque(quantidadeEstoque);
            
                controleFornecedor.adiciona(pessoaFisica);
                controleProduto.adiciona(produto);
                JOptionPane.showMessageDialog(rootPane, jTextFieldNome.getText() + " adicionado(a) com sucesso", "Sucesso", WIDTH, null);
                
                produto = new Produto();
               
                
       
}    
    
       else if (novoRegistro == true) {
            controleFornecedor.adicionar(umFornecedor);
        }
        modoAlteracao = false;
        novoRegistro = false;
        this.carregarListaFornecedores();
        this.habilitarDesabilitarCampos();
    
}



private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
    this.salvarRegistro();
}//GEN-LAST:event_jButtonSalvarActionPerformed

private void limparCampos() {
        jTextFieldBairro.setText(null);
        jTextField9CEP.setText(null);
        jTextFieldCidade.setText(null);
        jTextFieldComplemento.setText(null);
        jTextFieldCPF.setText(null);
        jTextFieldEstado.setText(null); 
        jTextFieldLogradouro.setText(null);
        jTextFieldNome.setText(null);
        jTextFieldNumero.setText("0");
        jTextFieldPais.setText(null);
        jTextFieldTelefoneResidencial.setText(null);
        jTextFieldTelefoneCelular.setText(null);
        jTextFieldNomeProduto.setText(null);
        jTextFieldValorCompra.setText("0");
        jTextFieldValorVenda.setText("0");
        
    
}

private void habilitarDesabilitarCampos() {
        jTextFieldBairro.setEnabled(modoAlteracao);
        jTextField9CEP.setEnabled(modoAlteracao);
        jTextFieldCidade.setEnabled(modoAlteracao);
        jTextFieldComplemento.setEnabled(modoAlteracao);
        jTextFieldCPF.setEnabled(modoAlteracao);
        jTextFieldLogradouro.setEnabled(modoAlteracao);
        jTextFieldNome.setEnabled(modoAlteracao);
        jTextFieldNumero.setEnabled(modoAlteracao);
        jTextFieldPais.setEnabled(modoAlteracao);
        jButtonNovo.setEnabled(modoAlteracao);
        jButtonPesquisar.setEnabled(modoAlteracao);
        jButtonSalvar.setEnabled(modoAlteracao);
    }

private void jTextFieldBairroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldBairroActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldBairroActionPerformed

private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed
    String pesquisa = JOptionPane.showInputDialog("Informe o nome do Fornecedor.");
    if (pesquisa != null) {
        this.pesquisarFornecedor(pesquisa);
    }        
}//GEN-LAST:event_jButtonPesquisarActionPerformed

    private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed
        // TODO add your handling code here:
        umFornecedor = null;
        this.limparCampos();
        modoAlteracao = true;
        this.habilitarDesabilitarCampos();
        this.jTextFieldNome.requestFocus();
    }//GEN-LAST:event_jButtonNovoActionPerformed

    private void jTableProdutoAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jTableProdutoAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_jTableProdutoAncestorAdded

    private void jTableProdutoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableProdutoMouseClicked
        if (jTableProduto.isEnabled()) {
        DefaultTableModel model = (DefaultTableModel) jTableProduto.getModel();
        String nome = (String) model.getValueAt(jTableProduto.getSelectedRow(), 0);
        this.pesquisarFornecedor(nome);
        }// TODO add your handling code here:
    }//GEN-LAST:event_jTableProdutoMouseClicked


    public JCheckBox getjCheckBox1() {
        return jCheckBoxCPF;
    }

    public void setjCheckBox1(JCheckBox jCheckBox1) {
        this.jCheckBoxCPF = jCheckBox1;
    }

    public JCheckBox getjCheckBox2() {
        return jCheckBoxCNPJ;
    }

    public void setjCheckBox2(JCheckBox jCheckBox2) {
        this.jCheckBoxCNPJ = jCheckBox2;
    }

    public JEditorPane getjEditorPane1() {
        return jEditorPane1;
    }

    public void setjEditorPane1(JEditorPane jEditorPane1) {
        this.jEditorPane1 = jEditorPane1;
    }

    public JLabel getjLabel2() {
        return jLabel2;
    }

    public void setjLabel2(JLabel jLabel2) {
        this.jLabel2 = jLabel2;
    }

    public JLabel getjLabel3() {
        return jLabel3;
    }

    public void setjLabel3(JLabel jLabel3) {
        this.jLabel3 = jLabel3;
    }

    public JLabel getjLabel4() {
        return jLabel4;
    }

    public void setjLabel4(JLabel jLabel4) {
        this.jLabel4 = jLabel4;
    }

    public JLabel getjLabel5() {
        return jLabel5;
    }

    public void setjLabel5(JLabel jLabel5) {
        this.jLabel5 = jLabel5;
    }

    public JLabel getjLabel6() {
        return jLabel6;
    }

    public void setjLabel6(JLabel jLabel6) {
        this.jLabel6 = jLabel6;
    }

    public JScrollPane getjScrollPane1() {
        return jScrollPane1;
    }

    public void setjScrollPane1(JScrollPane jScrollPane1) {
        this.jScrollPane1 = jScrollPane1;
    }

    public JTextField getjTextField2() {
        return jTextFieldTelefoneResidencial;
    }

    public void setjTextField2(JTextField jTextField2) {
        this.jTextFieldTelefoneResidencial = jTextField2;
    }

    public JTextField getjTextField3() {
        return jTextFieldTelefoneCelular;
    }

   
    public JTextField getjTextField4() {
        return jTextFieldCPF;
    }

    public void setjTextField4(JTextField jTextField4) {
        this.jTextFieldCPF = jTextField4;
    }
    
    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
   
    
private void pesquisarFornecedor(String nome) {
        Fornecedor FornecedorPesquisado = controleFornecedor.pesquisar(nome);

        if (FornecedorPesquisado == null) {
            exibirInformacao("Fornecedor não encontrado.");
        } else {
            this.umFornecedor = FornecedorPesquisado;
        }
    }


  private void jTableProduto(java.awt.event.MouseEvent evt) {                                                   
    if (jTableProduto.isEnabled()) {
        DefaultTableModel model = (DefaultTableModel) jTableProduto.getModel();
        String nome = (String) model.getValueAt(jTableProduto.getSelectedRow(), 0);
        this.pesquisarFornecedor(nome);
    }
}
  
  private void carregarListaFornecedores() {
        ArrayList<Fornecedor> listaFornecedores;
        listaFornecedores = controleFornecedor.getListaFornecedores();
        DefaultTableModel model = (DefaultTableModel) jTableProduto.getModel();
        model.setRowCount(0);
        for (Fornecedor b : listaFornecedores) {
           
            model.addRow(new String[]{b.getNome()});
        }
        jTableProduto.setModel(model);
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane Fornecedor;
    private javax.swing.JLabel ValorCompra;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JCheckBox jCheckBoxCNPJ;
    private javax.swing.JCheckBox jCheckBoxCPF;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelLogradouro;
    private javax.swing.JLabel jLabelValorVenda;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableProduto;
    private javax.swing.JTextField jTextField9CEP;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldCPF;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldComplemento;
    private javax.swing.JTextField jTextFieldDescricaoProduto;
    private javax.swing.JTextField jTextFieldEndereco;
    private javax.swing.JTextField jTextFieldEstado;
    private javax.swing.JTextField jTextFieldLogradouro;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNomeProduto;
    private javax.swing.JTextField jTextFieldNumero;
    private javax.swing.JTextField jTextFieldPais;
    private javax.swing.JTextField jTextFieldQuantidade;
    private javax.swing.JTextField jTextFieldTelefoneCelular;
    private javax.swing.JTextField jTextFieldTelefoneResidencial;
    private javax.swing.JTextField jTextFieldValorCompra;
    private javax.swing.JTextField jTextFieldValorVenda;
    // End of variables declaration//GEN-END:variables

    
}