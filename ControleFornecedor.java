/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import java.util.ArrayList;

/**
 *
 * @author alunos
 */
public class ControleFornecedor {
    private ArrayList<Fornecedor> listaFornecedores ;
 
    
    public ControleFornecedor(){
    listaFornecedores = new ArrayList<Fornecedor>();
    
    
}
    
    public void adicionar(Fornecedor umFornecedor) {
        listaFornecedores.add(umFornecedor);
    }
    
    public String adiciona(PessoaFisica fornecedor){
     
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Física adicionado com sucesso";
    
    }
    
    public String adiciona(PessoaJuridica fornecedor){
     
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Juridica adicionado com sucesso";
    
    }
    
    public Fornecedor pesquisar(String nome) {
        for (Fornecedor b: listaFornecedores) {
            if (b.getNome().equalsIgnoreCase(nome)) return b;
        }
        return null;
    }

    ArrayList<Fornecedor> getListaFornecedores() {
        return listaFornecedores;
    }
    
}
